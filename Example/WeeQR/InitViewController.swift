//
//  InitViewController.swift
//  WeeQR_Example
//
//  Created by Alejandro  Rodriguez on 8/19/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import WeeQR
class InitViewController: UIViewController, WeeQRDelegate {
    
    @IBOutlet weak var txtLabel: UILabel!
    
    lazy var vcQR: WeeQRControllerLogic  = {
        var vc = WeeQRFactory().getController()
        vc.delegate = self
        return vc
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func scanTap(_ sender: Any) {
        self.present((self.vcQR as! UIViewController), animated: true)
    }
    
    func getQR(_ code: String) {
        txtLabel.text = "Data: \(code)"
    }
    
    func getError(_ error: WeeQRError) {
        txtLabel.text = "Data: \(error.str)"
    }
    
}
