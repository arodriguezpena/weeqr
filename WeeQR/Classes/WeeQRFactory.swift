//
//  WeeQRFactory.swift
//  Pods-WeeQR_Example
//
//  Created by Alejandro  Rodriguez on 8/19/19.
//

import Foundation
import UIKit

/// Factory POD
public class WeeQRFactory: NSObject {
    
    /// Lector de código QR
    ///
    /// - Returns: ViewController Camera
    public func getController() -> WeeQRControllerLogic {
        let vc:WeeQRControllerLogic = WeeQRController()
        return vc
    }
}
