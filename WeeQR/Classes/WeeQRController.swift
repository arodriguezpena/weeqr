//
//  WeeQRController.swift
//  Pods-WeeQR_Example
//
//  Created by Alejandro  Rodriguez on 8/19/19.
//

import AVFoundation
import UIKit

/// Enumerado de códigos de error en la camara
public enum WeeQRError: String {
    case NO_COMPATIBLE = "Dispositivo no compatible"
    case NO_CAMERA = "Dispositivo no puede inicializar tú camara"
    public var str: String {
        return self.rawValue
    }
}

/// Delegado de respuesta codigo QR
public protocol WeeQRDelegate {
    func getQR(_ code: String) -> Void
    func getError( _ error : WeeQRError) -> Void
}

public protocol WeeQRControllerLogic {
    var delegate: WeeQRDelegate? {get set}
}

class WeeQRController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, WeeQRControllerLogic {

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var delegate:WeeQRDelegate?
    
    
    let exitButton: UIButton = {
        let btn = UIButton()
        btn.layer.cornerRadius = 25
        btn.setTitle("SALIR", for: .normal)
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 2.0
        btn.addTarget(self, action: #selector(exitTap), for: .touchUpInside)
        return btn
    }()
    
    @objc func exitTap(sender: UIButton!) {
        self.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCamera()
        setupDesing()
    }
    
    func setupDesing() {
        self.view.addSubview(exitButton)
        self.view.addContraintsWithFormat(format: "H:|-16-[v0]-16-|", views: exitButton)
        self.view.addContraintsWithFormat(format: "V:[v0(50)]-16-|", views: exitButton)
    }
    
    func setupCamera() {
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            delegate?.getError(.NO_COMPATIBLE)
            return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func failed() {
        delegate?.getError(.NO_CAMERA)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        delegate?.getQR(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}


fileprivate extension UIView {
    func addContraintsWithFormat(format: String, views: UIView...) {
        var viewDictionaty = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewDictionaty[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewDictionaty))
    }
    
}
