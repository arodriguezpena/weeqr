# WeeQR

[![CI Status](https://img.shields.io/travis/arodriguezp2003/WeeQR.svg?style=flat)](https://travis-ci.org/arodriguezp2003/WeeQR)
[![Version](https://img.shields.io/cocoapods/v/WeeQR.svg?style=flat)](https://cocoapods.org/pods/WeeQR)
[![License](https://img.shields.io/cocoapods/l/WeeQR.svg?style=flat)](https://cocoapods.org/pods/WeeQR)
[![Platform](https://img.shields.io/cocoapods/p/WeeQR.svg?style=flat)](https://cocoapods.org/pods/WeeQR)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WeeQR is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WeeQR'
```

## Author

arodriguezp2003, alejandrorodriguezpena@gmail.com

## License

WeeQR is available under the MIT license. See the LICENSE file for more info.
